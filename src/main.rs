extern crate cairo;
extern crate nalgebra as na;
extern crate num;
extern crate rand;
extern crate time;

// mod zillege;

use cairo::{Context, Format, ImageSurface};
use std::fs::File;

// use zillege::{Point, Points};

const OUTLINES: bool = false;

fn rotation(radians: f64) -> na::Matrix3<f64> {
    #[cfg_attr(rustfmt, rustfmt_skip)]
    na::Matrix3::new(
        radians.cos(), -radians.sin(), 0.,
        radians.sin(), radians.cos() , 0.,
        0.           , 0.            , 1.,
    )
}

fn translation(x: f64, y: f64) -> na::Matrix3<f64> {
    #[cfg_attr(rustfmt, rustfmt_skip)]
    na::Matrix3::new(
        1., 0., x ,
        0., 1., y ,
        0., 0., 1.,
    )
}

fn uniform_scale(scale: f64) -> na::Matrix3<f64> {
    #[cfg_attr(rustfmt, rustfmt_skip)]
    na::Matrix3::new(
        scale, 0.   , 0.,
        0.   , scale, 0.,
        0.   , 0.   , 1.
    )
}

fn main() {
    let now = time::now();
    let width = 600;
    let height = 600;

    let mut _radius = 0.;

    let surface = ImageSurface::create(Format::ARgb32, width as i32, height as i32)
        .expect("Could not create image");

    let context = Context::new(&surface);

    // background color
    context.rectangle(0., 0., width as f64, height as f64);
    context.set_source_rgb(239. / 255., 245. / 255., 213. / 255.);
    context.fill();

    // blue
    context.set_source_rgb(0., 0., 255.);

    // set (0, 0) as the origin
    context.translate((width / 2) as f64, (height / 2) as f64);

    let _360 = (360 as f64).to_radians();

    context.set_source_rgb(37. / 255., 10. / 255., 7. / 255.);
    context.arc(0., 0., 25., 0., _360);
    context.fill();

    context.set_source_rgb(176. / 255., 205. / 255., 245. / 255.);
    context.arc(0., 0., 10., 0., _360);
    context.fill();

    _radius += 25.;

    context.set_source_rgb(0., 0., 255.);

    let diamonds = 20;
    let diamond_radians = (360. / (diamonds as f64)).to_radians();
    let diamond_points = || {
        vec![
            na::Matrix3x1::new(0., 0., 1.),
            na::Matrix3x1::new(-0.16, 0.6, 1.),
            na::Matrix3x1::new(0., 0.9, 1.),
            na::Matrix3x1::new(0.16, 0.6, 1.),
        ]
    };

    context.set_source_rgb(39. / 255., 83. / 255., 43. / 255.);
    for i in 0..diamonds {
        let degrees = diamond_radians * (i as f64);
        let point_on_circle = (_radius * degrees.cos(), _radius * degrees.sin());
        let scale_factor = 42.;

        let rotate = degrees - (90 as f64).to_radians();

        let transformation = translation(point_on_circle.0, point_on_circle.1)
            * uniform_scale(scale_factor) * rotation(rotate);

        let pts = diamond_points();
        let mut diamond = pts.iter().map(|pt| transformation * pt);

        let first_pt = diamond.nth(0).unwrap();

        context.move_to(first_pt[0], first_pt[1]);
        for pt in diamond {
            context.line_to(pt[0], pt[1]);
        }
        context.line_to(first_pt[0], first_pt[1]);
        context.fill();
    }
    context.set_source_rgb(0., 0., 255.);

    _radius += 40.;
    if OUTLINES {
        context.arc(0., 0., _radius, 0., _360);
        context.stroke();
    }

    _radius += 20.;
    if OUTLINES {
        context.arc(0., 0., _radius, 0., _360);
        context.stroke();
    }

    for i in 0..48 {
        let _7_5 = (7.5 as f64).to_radians();
        let _2 = (4 as f64).to_radians();
        let _1 = (2 as f64).to_radians();
        let start_angle = _7_5 * (i as f64);
        let mid_angle = start_angle + _1;
        let end_angle = start_angle + _2;

        let length = 26.;
        let mid_offset = 3.8;

        context.set_source_rgb(7. / 255., 54. / 255., 116. / 255.);
        let _p = (_radius * start_angle.cos(), _radius * start_angle.sin());
        let p = (
            _p.0 + length * start_angle.cos(),
            _p.1 + length * start_angle.sin(),
        );

        let _p1 = (
            (_radius + mid_offset) * mid_angle.cos(),
            (_radius + mid_offset) * mid_angle.sin(),
        );
        let p1 = (
            (_radius + length + 1.) * mid_angle.cos(),
            (_radius + length + 1.) * mid_angle.sin(),
        );

        let _p2 = (_radius * end_angle.cos(), _radius * end_angle.sin());
        let p2 = (
            _p2.0 + length * end_angle.cos(),
            _p2.1 + length * end_angle.sin(),
        );

        context.move_to(_p.0, _p.1);
        context.line_to(_p1.0, _p1.1);
        context.line_to(_p2.0, _p2.1);
        context.line_to(p2.0, p2.1);
        context.line_to(p1.0, p1.1);
        context.line_to(p.0, p.1);
        context.line_to(_p.0, _p.1);
        context.fill();
    }

    context.set_source_rgb(0., 0., 255.);
    _radius += 30.;
    if OUTLINES {
        context.arc(0., 0., _radius, 0., _360);
        context.stroke();
    }

    _radius += 20.;
    if OUTLINES {
        context.arc(0., 0., _radius, 0., _360);
        context.stroke();
    }

    for i in 0..8 {
        let ten = (10 as f64).to_radians();
        let forty_five = (45 as f64).to_radians();
        let third = forty_five / 3.3;

        let start = (i as f64) * forty_five;
        let end = start + forty_five;
        let first_third = start + third;
        let last_third = end - third;

        // the code commented out below is unnecessary for the outlines
        // it might be useful later if we need a bottom border for the
        // weird shape thing
        // context.arc(0., 0., _radius, start, end);
        // context.stroke();

        let start_p1 = (_radius * start.cos(), _radius * start.sin());
        let start_p2 = (
            start_p1.0 + 30. * start.cos(),
            start_p1.1 + 30. * start.sin(),
        );

        if OUTLINES {
            context.move_to(start_p1.0, start_p1.1);
            context.line_to(start_p2.0, start_p2.1);
            context.stroke();
        }

        let end_p1 = (_radius * end.cos(), _radius * end.sin());
        let end_p2 = (end_p1.0 + 30. * end.cos(), end_p1.1 + 30. * end.sin());

        let middle_1 = start + (forty_five / 2.) - ten;
        let middle_1_p1 = (_radius * middle_1.cos(), _radius * middle_1.sin());
        let middle_1_p2 = (
            middle_1_p1.0 + 70. * middle_1.cos(),
            middle_1_p1.1 + 70. * middle_1.sin(),
        );

        let middle_2 = start + (forty_five / 2.) + ten;
        let middle_2_p1 = (_radius * middle_2.cos(), _radius * middle_2.sin());
        let middle_2_p2 = (
            middle_2_p1.0 + 70. * middle_2.cos(),
            middle_2_p1.1 + 70. * middle_2.sin(),
        );

        // the following is a bezier curve {{
        // start point

        if OUTLINES {
            context.move_to(start_p2.0, start_p2.1);
            context.curve_to(
                // control point 1
                middle_1_p2.0,
                middle_1_p2.1,
                // control point 2
                middle_2_p2.0,
                middle_2_p2.1,
                // end point
                end_p2.0,
                end_p2.1,
            );
            context.stroke();
        }
        // }}

        let circle_1_center_pt_1 = (_radius * first_third.cos(), _radius * first_third.sin());
        let circle_1_center = (
            circle_1_center_pt_1.0 + 25. * first_third.cos(),
            circle_1_center_pt_1.1 + 25. * first_third.sin(),
        );
        let circle_2_center_pt_1 = (_radius * last_third.cos(), _radius * last_third.sin());
        let circle_2_center = (
            circle_2_center_pt_1.0 + 25. * last_third.cos(),
            circle_2_center_pt_1.1 + 25. * last_third.sin(),
        );

        let shapes_fn = |x: f64, y: f64| {
            let shapes: f64 = 5.;
            let shape_radians = (360. / shapes).to_radians();
            let a = 0.3;
            let b = 0.4;
            let c = 0.0;
            let d = 0.6;
            let triangle_points = || {
                vec![
                    na::Matrix3x1::new(a, -c, 1.),
                    na::Matrix3x1::new(a, -b, 1.),
                    na::Matrix3x1::new(0., -d, 1.),
                    na::Matrix3x1::new(-a, -b, 1.),
                    na::Matrix3x1::new(-a, -c, 1.),
                ]
            };
            let shape_rotation = (120. as f64).to_radians();
            let shapes_points = || {
                let mut v = Vec::new();
                for i in 0..3 {
                    for pt in triangle_points() {
                        v.push(rotation(shape_rotation * (i as f64)) * pt);
                    }
                }
                v
            };
            for i in 0..(shapes as i32) {
                let angle = shape_radians * (i as f64) + (90 as f64).to_radians();
                let radius = 12.;
                let point_on_circle = (radius * angle.cos(), radius * angle.sin());
                let scale_factor = 12.;
                let transformation =
                    translation(point_on_circle.0, point_on_circle.1) * translation(x, y)
                        * uniform_scale(scale_factor) * rotation(angle);

                let mut pts = shapes_points().clone();
                let mut pts = pts.iter().map(|pt| transformation * pt);

                let first_pt = pts.nth(0).unwrap();

                context.move_to(first_pt[0], first_pt[1]);
                for pt in pts {
                    context.line_to(pt[0], pt[1]);
                }
                context.line_to(first_pt[0], first_pt[1]);
                context.stroke();
            }
            if OUTLINES {
                context.arc(x, y, 22., 0., _360);
                context.stroke();
            }
        };
        shapes_fn(circle_1_center.0, circle_1_center.1);
        shapes_fn(circle_2_center.0, circle_2_center.1);
    }

    _radius += 55.;
    if OUTLINES {
        for i in 0..16 {
            let radius = 40.;
            let distance = _radius + radius;
            let angle = (22.5 * (i as f64)).to_radians();
            let x = distance * angle.cos();
            let y = distance * angle.sin();
            context.arc(x, y, radius, 0., _360);
            context.stroke();
        }
    }
    let s_now = now.strftime("%d-%m-%y-%I-%M-%S").unwrap();

    std::fs::create_dir_all("progress").expect("couldn't create directory `progress`");

    let mut file =
        File::create(format!("progress/{}.png", s_now)).expect("Couldn’t create file.");

    surface
        .write_to_png(&mut file)
        .expect("Couldn’t write to png");
    println!("progress/{}.png", s_now);
}
