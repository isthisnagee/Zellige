# Zellige

An attempt at generating the following piece:

![](zellige-sample.jpg)

# Plan

The plan is as follows:

![](zellige-plan.jpg)

For each of the circled above, create each of the shapes inside,
then tile them as necessary. Then tile all the groupings to make the full image

A lot easier said than done

# Latest

![](current-zellige.png)
